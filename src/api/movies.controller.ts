import express, { Request, Response } from "express";
import moviesService from "./movies.service";

const router = express.Router();

router.use(express.json());

router.get("/id/:id", async (req: Request, res: Response) => {

    const id = req?.params?.id;
    try {
        let movie = await moviesService.getById(id);
        if (!movie)
            res.status(404).send(`No se encontró el película con id: ${id}`);
        else
            res.json(movie);
    } catch (error) {
        res.status(404).send(`No se encontró el película con id: ${id}`);
    }
});

//Retorna una lista de pelicualas de algun pais, y un genero que se encuentren entre las opciones recibidas.
//Con un puntaje de imdb mayor o igual al recibido o que haya ganado al menos la cantidad de premios especificada.

router.get("/find", async (req: Request, res: Response) => {

    let countries = typeof req?.query?.countries === "string" ? req.query.countries.split(",") : [];
    let genres = typeof req?.query?.genres === "string" ? req.query.genres.split(",") : [];
    let imdb = typeof req?.query?.imdb === "string" ? Number.parseFloat( req.query.imdb) : 0;
    let awardsWins = typeof req?.query?.awardsWins === "string" ? Number.parseFloat( req.query.awardsWins) : 0;

    if (!(countries instanceof Array) || !(genres instanceof Array) || imdb==0 || awardsWins==0)
        res.status(400).send();

    try {
        let movies = await moviesService.filter(countries, genres, imdb, awardsWins)

        res.json(movies);
    }
    catch (error) {
        res.status(404).send(`No se encontraron películas`);
    }
});

export default router;